## Test Task Project

This project is a test task for an interview. It is a JSON API written in Laravel 10 (Using Laravel Sail for
containerization). The API is a read endpoint for a single model called `User` which uses a mysql database running in
docker.

## Deployment

This project is based on Docker (using Laravel Sail). To get started, make sure Composer, Docker and Docker-Compose
are installed.(Composer is just to get Laravel Sail set up)

1. Clone the project to your local machine
2. Run `composer install` to install the dependencies (including Laravel Sail)
3. Run `cp .env.example .env` to create the .env file (you can change the database credentials in the .env file)
4. Run `./vendor/bin/sail artisan key:generate` to generate the application key
5. Run `./vendor/bin/sail up -d` to start the docker containers
6. Run `./vendor/bin/sail artisan migrate` to run the migrations
7. Run `./vendor/bin/sail artisan db:seed` to seed the database with dataset.csv.
    * This seeder is placed in the `database/seeders` directory.
    * To change the dataset, you need to change the `resources/dataset/dataset.csv` file and run the seeder again (in
      case you decide to change the dataset, make sure you run `./vendor/bin/sail artisan migrate:fresh` before seeding
      the database again)

## Usage

The API has a single endpoint which is a read endpoint for the `User` model. The endpoint is `/api/v1/users` and it
accepts the following query parameters:
Parameter | Description | Example
----------| ---------- | ----------
'page' | The page number of the results | 1         
'per_page' | The number of results per page | 10     
'category' | the category of the user | 'toys'    
'gender' | the gender of the user | 'male' or 'female'   
'age' | the age of the user | 20          
'age_range_from' | the lower bound of the age range of the user | 20
'age_range_to' | the upper bound of the age range of the user | 30
'birthDate' | the birth date of the user | '1990-01-01'    
'export' | if set to true, the results will be exported to a csv file | 1 or 0

(All parameters are optional)

# Examples

With CSV export:

`http://localhost/api/v1/users?page=1&per_page=10&gender=male&age_range_from=15&age_range_to=30&export=1`

Without CSV export:

`http://localhost/api/v1/users?page=1&per_page=10&gender=male&age_range_from=15&age_range_to=30&export=0`
