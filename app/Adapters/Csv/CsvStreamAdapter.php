<?php

namespace App\Adapters\Csv;

use Closure;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ArrayShape;

class CsvStreamAdapter implements CsvStreamAdapterContract
{

    /**
     * {@inheritDoc}
     */
    #[ArrayShape(['Cache-Control' => "string", 'Content-type' => "string", 'Content-Disposition' => "string", 'Expires' => "string", 'Pragma' => "string"])]
    public static function headers(string $fileName): array
    {
        $fileName = str_ends_with($fileName, '.csv') ? $fileName : $fileName . '.csv';

        return [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => "attachment; filename=$fileName",
            'Expires'             => '0',
            'Pragma'              => 'public',
        ];
    }


    /**
     * {@inheritDoc}
     */
    public function stream(Collection|array $list): Closure
    {
        $data = is_array($list) ? $list : $list->toArray();

        # add headers for each column in the CSV download
        array_unshift($data, array_keys($data[0]));

        return static function () use ($data) {
            $output = fopen('php://output', 'wb');
            foreach ($data as $row) {
                fputcsv($output, $row);
            }
            fclose($output);
        };
    }
}