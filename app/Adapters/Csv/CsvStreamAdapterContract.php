<?php

namespace App\Adapters\Csv;

use Closure;
use Illuminate\Support\Collection;

/**
 * Converts a collection of data to a CSV file stream
 */
interface CsvStreamAdapterContract
{
    /**
     * Get the header for the file name
     * @param string $fileName
     * @return string[]
     */
    public static function headers(string $fileName): array;

    /**
     * Get the body for the file as a stream on php://output
     * @param Collection|array $list
     * @return Closure
     */
    public function stream(Collection|array $list): Closure;
}