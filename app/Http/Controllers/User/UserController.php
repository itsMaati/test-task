<?php

namespace App\Http\Controllers\User;

use App\Adapters\Csv\CsvStreamAdapter;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\IndexRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Services\User\UserServiceContract;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpFoundation\StreamedResponse;

class UserController extends Controller
{
    private UserServiceContract $userService;
    private CsvStreamAdapter $csvStreamAdapter;

    public function __construct(UserServiceContract $userService, CsvStreamAdapter $csvStreamAdapter)
    {
        $this->userService = $userService;
        $this->csvStreamAdapter = $csvStreamAdapter;
    }

    public function index(IndexRequest $request): UserCollection|StreamedResponse
    {
        if ($request->get('export', false)) {
            return $this->download($this->userService->index($request, false));
        }

        return new UserCollection($this->userService->index($request));
    }

    private function download($users): StreamedResponse
    {
        return response()->stream(
            $this->csvStreamAdapter->stream($users), 200, $this->csvStreamAdapter->headers('users.csv')
        );

    }
}
