<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class IndexRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'page'           => ['integer', 'min:1'],
            'per_page'       => ['integer', 'min:1', 'max:100'],
            'category'       => ['sometimes', 'string'],
            'gender'         => ['sometimes', 'string', 'in:male,female'],
            'age'            => ['sometimes', 'integer', 'min:1', 'max:100'],
            'age_range_from' => ['required_with:age_range_to', 'integer', 'min:1', 'max:100'],
            'age_range_to'   => ['required_with:age_range_from', 'gt:age_range_from', 'integer', 'min:1', 'max:100'],
            'birthDate'      => ['sometimes', 'date_format:Y-m-d'],
            'export'         => ['nullable', 'bool'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
