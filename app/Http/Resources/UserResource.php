<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\User */
class UserResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'category'  => $this->category,
            'firstname' => $this->firstname,
            'lastname'  => $this->lastname,
            'email'     => $this->email,
            'gender'    => $this->gender,
            'birthDate' => $this->birthDate,
            //            'created_at'          => $this->created_at,
            //            'updated_at'          => $this->updated_at,
        ];
    }
}
