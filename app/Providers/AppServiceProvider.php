<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind(
            \App\Services\User\UserServiceContract::class,
            \App\Services\User\UserService::class
        );
        $this->app->bind(
            \App\Adapters\Csv\CsvStreamAdapterContract::class,
            \App\Adapters\Csv\CsvStreamAdapter::class
        );

    }
}
