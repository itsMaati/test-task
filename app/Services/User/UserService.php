<?php

namespace App\Services\User;

use App\Models\User;
use Illuminate\Http\Request;

class UserService implements UserServiceContract
{

    public function index(Request $request, bool $paginate = true)
    {
        $query = User::query();
        if ($request->has('category')) {
            $query->where('category', $request->get('category'));
        }
        if ($request->has('gender')) {
            $query->where('gender', $request->get('gender'));
        }
        if ($request->has('birthDate')) {
            $query->where('birthDate', $request->get('birthDate'));
        }
        if ($request->has('age')) {
            $query->whereRaw('TIMESTAMPDIFF(YEAR, birthDate, CURDATE()) = ?', [$request->get('age')]);
        }
        if ($request->has('age_range_from') && $request->has('age_range_to')) {
            $query->whereRaw('TIMESTAMPDIFF(YEAR, birthDate, CURDATE()) BETWEEN ? AND ?', [$request->get('age_range_from'), $request->get('age_range_to')]);
        }

        return $paginate ? $query->paginate(
            perPage: $request->get('per_page', 10),
            page: $request->get('page', 1)
        ) : $query->get();


    }
}
