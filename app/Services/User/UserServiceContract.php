<?php

namespace App\Services\User;

use Illuminate\Http\Request;

interface UserServiceContract
{
    public function index(Request $request);
}
