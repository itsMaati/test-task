<?php

namespace Database\Seeders;

use App\Models\User;
use DateTime;
use Illuminate\Database\Seeder;

class DatasetSeeder extends Seeder
{
    private $chunkSize = 10;

    /**
     * @throws \Exception
     */
    public function run()
    {
        // Open the CSV file for reading
        $file = fopen(resource_path('dataset/dataset.csv'), 'rb');

        // Read the header row
        $headers = fgetcsv($file);

        // Initialize an empty array to hold the values
        $values = [];

        // Loop through the rows of the CSV file
        while ($row = fgetcsv($file)) {
            // Combine the header names with the row data
            $data = array_combine($headers, $row);

            // Convert birth date to a DateTime object
            $data['birthDate'] = new DateTime($data['birthDate']);

            // Add the values to the array
            $values[] = $data;

            // If the array has reached the chunk size, insert the chunk into the database
            if (count($values) === $this->chunkSize) {
                User::insert($values);

                // Reset the array
                $values = [];
            }
        }

        // Insert any remaining rows
        if (!empty($values)) {
            User::insert($values);
        }

        // Close the file
        fclose($file);

    }
}
